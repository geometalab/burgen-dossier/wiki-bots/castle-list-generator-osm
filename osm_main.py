from castle_query_osm import query_osm_castles, OverpassQueryError
from missing_wikidata_tag_processor import process_castle_json
from add_text_to_wiki import add_text_to_wiki

TEXTFILE = 'wiki/osm_missing_wd_tag_table'
WIKIPAGE = u'Wikimedia CH/Burgen-Dossier/FehlendeWikidataReferenzen'


def main():
    try:
        query_osm_castles()
    except OverpassQueryError:
        return

    process_castle_json()
    add_text_to_wiki(TEXTFILE, WIKIPAGE)


if __name__ == "__main__":
    main()
