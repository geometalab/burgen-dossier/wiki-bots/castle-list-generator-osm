# OSM Castle Query

[![build status](https://gitlab.com/geometalab/castle-list-generator-osm/badges/master/pipeline.svg)](https://gitlab.com/geometalab/castle-list-generator-osm/commits/master)

The scripts in this repository query OSM for castles with missing Wikidata tags and publish the results [here](https://meta.wikimedia.org/wiki/Wikimedia_CH/Burgen-Dossier/FehlendeWikidataReferenzen).

![Overview](img/osm_query_overview.png)

## Overview

In the following, the scripts involved in the process are listed and briefly described to provide an overview of the application.

* **castle\_query\_osm.py**: This script queries the [Overpass API](https://wiki.openstreetmap.org/wiki/Overpass_API) for nodes, ways and relations with tag `castle=historic` in the region `(45.6, 5.4, 47.99, 11.2)` (Switzerland plus surroundings). The resulting JSON output can be downloaded [here](https://gitlab.com/geometalab/castle-list-generator-osm/-/jobs/artifacts/master/raw/dumps/osm_castles.json?job=deploy).

* **missing\_wikidata\_tag\_processor.py**: This script filters the JSON output for results missing the Wikidata reference entry and creates a file containing these entries in wiki syntax. The file can be downloaded [here](https://gitlab.com/geometalab/castle-list-generator-osm/-/jobs/artifacts/master/raw/wiki/osm_missing_wd_tag_table?job=deploy).

* **add\_text\_to\_wiki.py**: This script publishes the contents of the previously produced file on [Wikimedia](https://meta.wikimedia.org/wiki/Wikimedia_CH/Burgen-Dossier/FehlendeWikidataReferenzen) using a shared [PyWikiBot](https://www.mediawiki.org/wiki/Manual:Pywikibot) with username `OSMCastleBot`. If the bots behaviour causes issues, please create an issue on this repository's issue page.

* **osm\_main.py**: This script executes the above features in the order described. The CI job for this repository is scheduled to run every hour on `HH:42`. The linked artifacts are updated according to this schedule.

## Contribution

If you think you have a valuable contribution, feel free to open a pull request.
