import json
import os

INPUT_PATH = 'dumps'
INPUT_FILE = 'osm_castles.json'

WIKI_PATH = 'wiki'
WIKI_FILE = 'osm_missing_wd_tag_table'


def has_no_wd_tag(castle):
    if 'wikidata' in castle['tags']:
        return False
    else:
        return True


def headers():
    return ['! ID\n', '! Type\n', '! Name\n']


def castle_rows(castles_missing_wd_tags):
    castle_entries = []
    for castle in castles_missing_wd_tags:
        osm_type = ''
        try:
            osm_type = castle["type"]
            osm_type_string = f'| {osm_type}\n'
        except KeyError:
            osm_type_string = '| style="background-color: #FFEBAD" | -\n'
        try:
            identifier = f'| [https://www.openstreetmap.org/{osm_type}/{str(castle["id"])} {str(castle["id"])}]\n'
        except KeyError:
            identifier = '| style="background-color: #FFEBAD" | -\n'
        try:
            name = f'| {castle["tags"]["name"]} \n'
        except KeyError:
            name = '| style="background-color: #FFEBAD" | -\n'
        castle_entries.append([identifier, osm_type_string, name, '|-\n'])
    return [row for sublist in castle_entries for row in sublist]


def process_castle_json():
    with open(f'{INPUT_PATH}/{INPUT_FILE}', 'r') as input_json:
        osm_dump = json.load(input_json)

    castles = osm_dump['elements']
    castles_missing_wd_tags = list(filter(has_no_wd_tag, castles))

    if not os.path.exists(WIKI_PATH):
        os.mkdir(WIKI_PATH, 0o755)

    with open(f'{WIKI_PATH}/{WIKI_FILE}', 'w+') as output:
        output.writelines(
            ['Die nachfolgende Tabelle enthält sämtliche OSM Nodes, Ways und Relations, die Burgen beschreiben und '
             'keine Wikidata Referenz aufweisen. Die Query beschränkt sich auf die OSM Region <code>(45.6, 5.4, '
             '47.99, 11.2)</code> und die Tags '
             '<code>[historic=castle]</code>, '
             '<code>[historic=tower]</code> und '
             '<code>[historic=archaeological_site]</code> kombiniert mit '
             '<code>[site_type=fortification]</code>.\n\n\n\n'
             '{{Caution|Diese Seite wird automatisch aktualisiert mittels eines stündlich ablaufenden Scripts. Das '
             'manuelle Updaten von Tabelleneinträgen lohnt sich deshalb nicht, weil Änderungen wieder überschrieben '
             'werden. Sollten Probleme mit dem Skript auftreten, so können Issues im ['
             'https://gitlab.com/geometalab/castle-list-generator-osm Repository] erfasst werden.}}\n\n\n\n']
        )
        output.writelines(
            ['{| class="wikitable"\n',
             '|+ Caption: OSM Burgen mit fehlenden Wikidata Referenzen\n',
             '|-\n'])
        output.writelines(headers())
        output.writelines('|-\n')
        output.writelines(castle_rows(castles_missing_wd_tags))
        output.writelines('|}')
